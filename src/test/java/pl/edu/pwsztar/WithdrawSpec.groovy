package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class WithdrawSpec extends Specification {

    @Unroll
    def "should withdraw #cashToWithdraw from account #accountNumber"(){
        given: "initial data"
            def bank = TestData.getTestBank()
        when: "withdraw cash"
            def result = bank.withdraw(accountNumber, cashToWithdraw)
        then: "check if withdraw was successful"
            result

        where:
            accountNumber | cashToWithdraw
            1             | 50
            2             | 90
            3             | 120
    }
}
