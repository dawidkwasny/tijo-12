package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class TransferSpec extends Specification {

    @Unroll
    def "should transfer from #fromAccount to #toAccount account #cash cash"(){
        given: "initial data"
            def bank = TestData.getTestBank()
        when: "transfer money"
            def result = bank.transfer(fromAccount, toAccount, cash)
        then: "check if transfer was successful"
            result

        where:
            fromAccount | toAccount | cash
            1           | 2         | 30
            2           | 1         | 100
            3           | 2         | 150

    }
}
