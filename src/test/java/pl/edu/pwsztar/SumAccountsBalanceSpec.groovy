package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class SumAccountsBalanceSpec extends Specification {

    @Unroll
    def "should return sum from all accounts"(){
        given: "initial data"
            def bank = TestData.getTestBank()
        when: "sum balance from all accounts"
            def sum = bank.sumAccountsBalance()
        then: "check sum"
            sum == 300

    }
}
