package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class DeleteAccountSpec extends Specification {

    @Unroll
    def "should delete account number #accountNumber with cash #expected"() {
        given: "initial data"
            def bank = TestData.getTestBank()
        when: "account deleted"
            def balance = bank.deleteAccount(accountNumber)
        then: "check deleted account cash"
            balance == expected

        where:
            accountNumber | expected
            1             | 50
            2             | 100
            3             | 150
    }
}
