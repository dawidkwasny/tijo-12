package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;

class Bank implements BankOperation {

    private static int accountNumber = 0;
    private final List<Account> accounts = new ArrayList<Account>();

    public int createAccount() {
        accountNumber += 1;
        accounts.add(new Account(accountNumber, 0));
        return accountNumber;
    }

    public int deleteAccount(int accountNumber) {
        int balance = ACCOUNT_NOT_EXISTS;
        Account account = findAccount(accountNumber);
        if(account.getAccountNumber() != ACCOUNT_NOT_EXISTS) {
            balance = account.getBalance();
            accounts.remove(account);
        }
        return balance;
    }

    public boolean deposit(int accountNumber, int amount) {
        Account account = findAccount(accountNumber);
        if(account.getAccountNumber() != ACCOUNT_NOT_EXISTS && amount > 0) {
            account.setBalance(account.getBalance() + amount);
            return true;
        }
        return false;
    }

    public boolean withdraw(int accountNumber, int amount) {
        Account account = findAccount(accountNumber);
        if(account.getAccountNumber() != ACCOUNT_NOT_EXISTS && amount > 0 && account.getBalance() >= amount) {
            account.setBalance(account.getBalance() - amount);
            return true;
        }
        return false;
    }

    public boolean transfer(int fromAccount, int toAccount, int amount) {
        Account from = findAccount(fromAccount);
        Account to = findAccount(toAccount);
        if(from.getAccountNumber() != ACCOUNT_NOT_EXISTS && to.getAccountNumber() != ACCOUNT_NOT_EXISTS && from.getBalance() >= amount && amount > 0) {
            from.setBalance(from.getBalance() - amount);
            to.setBalance(to.getBalance() + amount);
            return true;
        }
        return false;
    }

    public int accountBalance(int accountNumber) {
        Account account = findAccount(accountNumber);
        if(account.getAccountNumber() != ACCOUNT_NOT_EXISTS) {
            return account.getBalance();
        }
        return ACCOUNT_NOT_EXISTS;
    }

    public int sumAccountsBalance() {
        int sum = 0;
        for(Account account : accounts) {
            sum += account.getBalance();
        }
        return sum;
    }

    private Account findAccount(int accountNumber) {
        for(Account account : accounts) {
            if(account.getAccountNumber() == accountNumber) {
                return account;
            }
        }
        return new Account(ACCOUNT_NOT_EXISTS, 0);
    }
}
