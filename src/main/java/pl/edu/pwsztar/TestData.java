package pl.edu.pwsztar;

public class TestData {

    private static final Bank bank = initBank();

    private static Bank initBank() {
        Bank bank = new Bank();
        bank.createAccount();
        bank.createAccount();
        bank.createAccount();
        bank.deposit(1, 50);
        bank.deposit(2, 100);
        bank.deposit(3, 150);
        return bank;
    }
    public static Bank getTestBank() {
        return bank;
    }
}
